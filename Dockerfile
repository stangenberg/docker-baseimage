# Docker friendly ubuntu 14.04 base image 
# See https://github.com/phusion/baseimage-docker
FROM phusion/baseimage:0.9.16

MAINTAINER Thorben Stangenberg <thorben@stangenberg.net>

# noninteractive debian frontend as default 
ENV DEBIAN_FRONTEND=noninteractive

# Ensure we create the container with UTF-8 locale
RUN echo 'LANG="en_US.UTF-8"' > /etc/default/locale && \
	locale-gen en_US.UTF-8        

# limited supported english languages
RUN echo "en_US.UTF-8 UTF-8" > /var/lib/locales/supported.d/en

# dont deactivate sshd
RUN rm -f /etc/service/sshd/down

# Regenerate SSH host keys. baseimage-docker does not contain any, so you
# have to do that yourself. You may also comment out this instruction; the
# init system will auto-generate one during boot.
RUN /etc/my_init.d/00_regen_ssh_host_keys.sh

# expose sshd port
EXPOSE 22

# Install the SSH public key
RUN curl https://bitbucket.org/thorben/public-ssh/raw/master/thorben@stangenberg.net.pub -o /root/.ssh/authorized_keys