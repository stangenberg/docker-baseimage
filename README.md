# Docker Baseimage

It uses [Phusion's Baseimage][Phusion] and installs my ssh key for easy access.


## Features ##

- [Phusion's Baseimage][Phusion] 
- UTF-8 Charset


## Exposed volumes ##

None.


## Exposed ports ##

- 22 / SSH 


## Environment Variables

- `DEBIAN_FRONTEND` - set to `noninteractive` as default - [Debconf Manpage](http://manpages.ubuntu.com/manpages/trusty/man7/debconf.7.html)


## Usage ##

Use it as base image for other containers.


## License ##

[Published under the MIT License][LICENSE]


[Phusion]: http://phusion.github.io/baseimage-docker/ "Phusion's Baseimage"
[LICENSE]: https://bitbucket.org/thstangenberg/docker-baseimage/src/master/LICENSE.md "Published under the MIT License"